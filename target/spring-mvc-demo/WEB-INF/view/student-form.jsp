<%--
  Created by IntelliJ IDEA.
  User: BG
  Date: 07-Mar-17
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Save Student</title>

    <link type="text/css" rel="stylesheet" href="{pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="{pageContext.request.contextPath}/resources/css/add-student-style.css">
</head>
<body>
    <div id="wrapper">
        <div id="header">
            <h2>Student List</h2>
        </div>
    </div>

    <div id="container">
        <h3>Save Student</h3>

        <form:form action="saveStudent" modelAttribute="student" method="POST">

            <table>
                <tbody>
                    <tr>
                        <td><label>First Name:</label></td>
                        <td><form:input path="firstName"/></td>
                    </tr>
                    <tr>
                        <td><label>Last Name:</label></td>
                        <td><form:input path="lastName"/></td>
                    </tr>
                    <tr>
                        <td><label>Email:</label></td>
                        <td><form:input path="email"/></td>
                    </tr>
                    <tr>
                        <td><label></label></td>
                        <td><input type="submit" value="Save" class="save"></td>
                    </tr>
                </tbody>
            </table>
        </form:form>

        <div>
            <p>
                <a href="${pageContext.request.contextPath}/student/list">Back to Student List</a>
            </p>
        </div>

    </div>
</body>
</html>
