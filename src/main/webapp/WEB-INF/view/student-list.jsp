<%--
  Created by IntelliJ IDEA.
  User: BG
  Date: 27-Feb-17
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List of Students</title>

    <link type="text/css"
          rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>

    <div id="wrapper">
        <div id="header">
            <h2>List of University Students</h2>
        </div>
    </div>

    <div id="container">

        <div id="content">

            <!-- adding button Add Student -->
            <input type="button" value="Add Student"
                onclick="window.location.href='showFormForAdd'; return false;"
                class="add-button"
            />

            <table>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                </tr>

                <c:forEach var="tempStudent" items="${students}">
                    <tr>
                        <td>${tempStudent.firstName}</td>
                        <td>${tempStudent.lastName}</td>
                        <td>${tempStudent.email}</td>
                    </tr>
                </c:forEach>

            </table>
        </div>
    </div>
</body>
</html>
