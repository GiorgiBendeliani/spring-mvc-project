package com.bendeliani.springdemo.service;

import com.bendeliani.springdemo.entity.Student;

import java.util.List;

/**
 * Created by BG on 07-Mar-17.
 */
public interface StudentService {
    public List<Student> getStudents();

    public void saveStudent(Student theStudent);
}
