package com.bendeliani.springdemo.DAO;

import com.bendeliani.springdemo.entity.Student;

import java.util.List;

/**
 * Created by BG on 28-Feb-17.
 */
public interface StudentDAO {

    public List<Student> getStudents();

    public void saveStudent(Student theStudent);
}
