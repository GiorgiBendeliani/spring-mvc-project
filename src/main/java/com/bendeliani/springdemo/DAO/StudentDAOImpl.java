package com.bendeliani.springdemo.DAO;

import com.bendeliani.springdemo.entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by BG on 28-Feb-17.
 */

@Repository
public class StudentDAOImpl implements StudentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Student> getStudents() {

        // get the current hibernate session
        Session currentSession = sessionFactory.getCurrentSession();

        // create a query
        Query<Student> theQuery =
                currentSession.createQuery("from students order by lastName", Student.class);

        // get the list of Students from the query
        List<Student> students = theQuery.getResultList();

        // return the retrieved list
        return students;
    }

    @Override
    public void saveStudent(Student theStudent) {

        Session currentSession = sessionFactory.getCurrentSession();

        currentSession.save(theStudent);

    }
}
