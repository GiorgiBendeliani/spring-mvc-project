package com.bendeliani.springdemo.controller;

import com.bendeliani.springdemo.DAO.StudentDAO;
import com.bendeliani.springdemo.entity.Student;
import com.bendeliani.springdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by BG on 27-Feb-17.
 */

@Controller
@RequestMapping("/student")
public class StudentController {

    // inject the student service
    @Autowired
    private StudentService studentService;

    @GetMapping("/list")
    public String listStudents(Model theModel){

        // get the students from Service
         List theStudents = studentService.getStudents();

        // add theStudents to the model
        theModel.addAttribute("students", theStudents);

        return "student-list";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel){

        Student theStudent = new Student();

        theModel.addAttribute("student", theStudent);

        return "student-form";
    }

    @PostMapping("/saveStudent")
    public String saveStudent(@ModelAttribute("student") Student theStudent){

        studentService.saveStudent(theStudent);

        return "redirect:/student/list/";
    }



}
